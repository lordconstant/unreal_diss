// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "LeverActor.generated.h"

/**
 * 
 */
UCLASS()
class MP_WHITEBOX_API ALeverActor : public AActor
{
	GENERATED_UCLASS_BODY()

	USceneComponent* arrowComp;

	bool m_inputKeyPressed;

	bool m_usedLever;
public:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = StaticMeshComponent)
		UStaticMeshComponent* staticMeshComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = TriggerArea)
		USphereComponent* triggerArea;

	UFUNCTION(BlueprintCallable, Category=Interaction)
	bool interactWithLever(AActor* player);
	void inputKeyPressed();
	
	virtual void BeginPlay() override;
};
