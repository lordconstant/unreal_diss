// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "EarthAbilitySet.h"
#include "WindAbilitySet.h"
#include "FireAbilitySet.h"
#include "GameFramework/Character.h"
#include "ElementalCharacter.generated.h"

UCLASS(config = Game)
class MP_WHITEBOX_API AElementalCharacter : public ACharacter
{
	GENERATED_UCLASS_BODY()

	UAbilitySet* activeAbilities;
	
	//int ActiveAbility;
protected:

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/**
	* Called via input to turn at a given rate.
	* @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	*/
	void TurnAtRate(float Rate);

	/**
	* Called via input to turn look up/down at a given rate.
	* @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	*/
	void LookUpAtRate(float Rate);

	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);

	void EarthPillar();
	void releaseRightMouseButton();

	void changeToWind();
	void changeToEarth();
	void changeToFire();
protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
	// End of APawn interface
	
	virtual void Tick(float DeltaSeconds) override;

public:
	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		UCameraComponent* FollowCamera;

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseLookUpRate;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Abilities)
		UWindAbilitySet* windAbilities;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Abilities)
		UEarthAbilitySet* earthAbilities;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Abilities)
		UFireAbilitySet* fireAbilities;
};
