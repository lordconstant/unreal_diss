// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Ability.h"
#include "Components/ActorComponent.h"
#include "AbilitySet.generated.h"

enum POWERS{ WINDPOWER = 0, EARTHPOWER = 1, FIREPOWER = 2, ELEPOWERCOUNT = 3};

/**
 * 
 */
UCLASS()
class MP_WHITEBOX_API UAbilitySet : public UActorComponent
{
	GENERATED_UCLASS_BODY()

	TArray<UAbility*> Abilities;
public:
	virtual void init();

	virtual void startAbility(uint32 ability, AActor* player);
	virtual void useAbility(uint32 ability, AActor* player);
	UFUNCTION(Reliable, Server, WithValidation)
	virtual void endAbility(uint32 ability, AActor* player);
	virtual void stopAbility(uint32 ability);

	virtual bool isChanneled(uint32 ability);
	virtual void isChanneled(uint32 ability, bool channeling);
};
