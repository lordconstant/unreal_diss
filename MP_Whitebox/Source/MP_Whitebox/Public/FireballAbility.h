// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Ability.h"
#include "FireballAbility.generated.h"

/**
 * 
 */
UCLASS()
class MP_WHITEBOX_API UFireballAbility : public UAbility
{
	GENERATED_BODY()

	float m_cooldownTimer;
	bool m_canSpawnProjectile;
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AbilityProperties)
		TSubclassOf<AActor> fireBall;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AbilityProperties)
		float cooldownTime;

	void startAbility(AActor* player);
	void useAbility(AActor* player);
	void endAbility(AActor* player);
	void stopAbility();

	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction) override;
};
