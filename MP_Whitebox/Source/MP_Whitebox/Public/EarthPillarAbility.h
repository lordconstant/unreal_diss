// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "DrawDebugHelpers.h"
#include "Ability.h"
#include "EarthPillarAbility.generated.h"

/**
 * 
 */
UCLASS()
class MP_WHITEBOX_API UEarthPillarAbility : public UAbility
{
	GENERATED_UCLASS_BODY()

	TArray<AActor*> m_spawningPillars;
	TArray<AActor*> m_pillars;
	TArray<AActor*> m_groundParticles;

	bool m_showingMarker;
	bool m_canSpawn;
	bool m_spawnFailed;
	bool m_water;

	AActor* m_curMarker;

	float m_cooldownTimer;
	float m_failTimer;

	UMaterialInstanceDynamic* m_dynamMat;

	FRotator m_pillRot;
public:
	//UEarthPillarAbility();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AbilityProperties)
		TSubclassOf<AActor> PillarMarker;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AbilityProperties)
		TSubclassOf<AActor> PillarGroundParticle;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AbilityProperties)
		TSubclassOf<AActor> WaterGroundParticle;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AbilityProperties)
		TSubclassOf<AActor> Pillar;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AbilityProperties)
		int32 MaxPillars;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AbilityProperties)
		float CooldownTime;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AbilityProperties)
		FColor MarkerDefaultColor;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AbilityProperties)
		FColor MarkerFailColor;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AbilityProperties)
		float MarkerFailTime;

	void startAbility(AActor* player);
	void useAbility(AActor* player);
	UFUNCTION(Reliable, Server, WithValidation)
	void endAbility(AActor* player);
	void stopAbility();

	//Function is replicated over the server, Moves the pillar up from underground
	UFUNCTION(Reliable, Server, WithValidation)
	void spawnPillar(uint32 num, float deltaTime);
	//Function is replicated over the server, Moves the pillar back underground (also handles deletion of pillars)
	UFUNCTION(Reliable, Server, WithValidation)
	void removePillar(uint32 num, float deltaTime);

	void changeMarkerColor(FColor markerCol);

	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction) override;
};
