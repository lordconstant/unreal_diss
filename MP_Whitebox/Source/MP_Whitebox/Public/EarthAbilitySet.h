// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "EarthPillarAbility.h"
#include "AbilitySet.h"
#include "EarthAbilitySet.generated.h"

/**
 * 
 */

enum EARTHPOWERS{EARTHPILLAR = 0};


UCLASS()
class MP_WHITEBOX_API UEarthAbilitySet : public UAbilitySet
{
	GENERATED_UCLASS_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Abilities)
		UEarthPillarAbility* EarthPillarAbility;

	void initialisePowers(const class FObjectInitializer& PCIP, AActor* parent);
	void init();

	void startAbility(uint32 ability, AActor* player);
	void useAbility(uint32 ability, AActor* player);
	void endAbility(uint32 ability, AActor* player);
	void stopAbility(uint32 ability);

	bool isChanneled(uint32 ability);
	void isChanneled(uint32 ability, bool channeling);
};
