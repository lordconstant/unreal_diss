// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "WindPushAbility.h"
#include "AbilitySet.h"
#include "WindAbilitySet.generated.h"

/**
 * 
 */

enum WINDPOWERS{ WINDPUSH = 0 };

UCLASS()
class MP_WHITEBOX_API UWindAbilitySet : public UAbilitySet
{
	GENERATED_UCLASS_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Abilities)
	UWindPushAbility* WindPushAbility;

	void initialisePowers(const class FObjectInitializer& PCIP, AActor* parent);
	void init();

	void startAbility(uint32 ability, AActor* player);
	void useAbility(uint32 ability, AActor* player);
	void endAbility(uint32 ability, AActor* player);
	void stopAbility(uint32 ability);

	bool isChanneled(uint32 ability);
	void isChanneled(uint32 ability, bool channeling);
};
