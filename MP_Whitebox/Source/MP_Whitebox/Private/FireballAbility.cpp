// Fill out your copyright notice in the Description page of Project Settings.

#include "MP_Whitebox.h"
#include "FireballAbility.h"

void UFireballAbility::startAbility(AActor* player){
	m_cooldownTimer = 0.0f;
	m_canSpawnProjectile = false;
}

void UFireballAbility::useAbility(AActor* player){
}

void UFireballAbility::endAbility(AActor* player){
	if (m_canSpawnProjectile && fireBall){
		UWorld* myWorld = player->GetWorld();

		if (myWorld){
			FVector spawnPoint, rotNorm;
			FRotator tempRot;

			//Spawns an actor infront of the player at the cast distance
			player->GetInstigatorController()->GetPlayerViewPoint(spawnPoint, tempRot);
			rotNorm = tempRot.Vector();
			rotNorm.Normalize();
			spawnPoint = player->GetActorLocation();
			spawnPoint += (rotNorm * CastDistance);
			(AActor*)myWorld->SpawnActor(fireBall, &spawnPoint, &tempRot);

			m_cooldownTimer = 0.0f;
		}
	}
}

void UFireballAbility::stopAbility(){
}

void UFireballAbility::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction){
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	m_cooldownTimer += DeltaTime;

	if (m_cooldownTimer >= cooldownTime){
		m_canSpawnProjectile = true;
	}
	else{
		m_canSpawnProjectile = false;
	}
}