// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

#include "MP_Whitebox.h"
#include "MP_WhiteboxGameMode.h"
#include "MP_WhiteboxCharacter.h"

AMP_WhiteboxGameMode::AMP_WhiteboxGameMode(const class FObjectInitializer& PCIP)
	: Super(PCIP)
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/MyCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
