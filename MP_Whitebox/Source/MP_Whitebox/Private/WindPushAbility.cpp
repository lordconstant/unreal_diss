// Fill out your copyright notice in the Description page of Project Settings.

#include "MP_Whitebox.h"
#include "WindPushAbility.h"

UWindPushAbility::UWindPushAbility(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
}

void UWindPushAbility::startAbility(AActor* player){
	if (!m_curMarker){
		FHitResult hitRes(ForceInit);
		FCollisionQueryParams traceParams((FName)"WindPushMarkerTrace", true);

		traceParams.bTraceComplex = true;
		traceParams.bTraceAsyncScene = true;
		traceParams.bReturnPhysicalMaterial = false;
		traceParams.AddIgnoredActor(player);

		FVector startPos, endPos;

		UWorld* myWorld = player->GetWorld();
		
		startPos = player->GetActorLocation();
		endPos = startPos - (player->GetActorUpVector() * 1000.0f);

		//Fire a ray down to the ground from the centre of the player
		if (myWorld->LineTraceSingle(hitRes, startPos, endPos, ECC_WorldStatic, traceParams)){
			if (hitRes.GetActor()){
				//Spawns the wind marker or moves it if it exists
				if (!m_showingMarker){
					FRotator tempRot = player->GetActorForwardVector().Rotation();
					m_curMarker = (AActor*)myWorld->SpawnActor(WindMarker, &hitRes.ImpactPoint, &tempRot);
					m_showingMarker = true;
				}
				else{
					if (m_curMarker){
						m_curMarker->SetActorLocation(hitRes.ImpactPoint);
						m_curMarker->SetActorRotation(player->GetActorForwardVector().Rotation());
					}
				}
			}
		}
	}
}

void UWindPushAbility::useAbility(AActor* player){
	FHitResult hitRes(ForceInit);
	FCollisionQueryParams traceParams((FName)"WindTrace", true);
	FCollisionObjectQueryParams sweepObjectParams;

	traceParams.bTraceComplex = true;
	traceParams.bTraceAsyncScene = true;
	traceParams.bReturnPhysicalMaterial = false;
	traceParams.AddIgnoredActor(player);

	FVector startPos, endPos;

	UWorld* myWorld = player->GetWorld();

	//Checks to see if the ability is being cast
	if (isChanneled()){
		if (WindParticles){
			FVector tempPos = player->GetActorLocation();
			FRotator tempRot = player->GetActorForwardVector().Rotation();

			//Spawns in the particle effect or reactivates it
			if (!m_windParticles){
				m_windParticles = (AActor*)myWorld->SpawnActor(WindParticles, &tempPos, &tempRot);
			}
			else{
				TArray<UActorComponent*> comps;

				m_windParticles->GetComponents(comps);

				for (int i = 0; i < comps.Num(); ++i){
					UParticleSystemComponent* particleComp = Cast<UParticleSystemComponent>(comps[i]);

					if (particleComp){
						particleComp->Activate();
					}
				}

				m_windParticles->SetActorLocation(tempPos);
				m_windParticles->SetActorRotation(tempRot);
			}
		}

		startPos = player->GetActorLocation();
		endPos = startPos + (player->GetActorForwardVector() * CastDistance);

		//Does a sphereSweep in the direction the player is facing
		if (myWorld->SweepSingle(hitRes, startPos, endPos, FQuat::Identity, FCollisionShape::MakeSphere(50.0f), traceParams, sweepObjectParams)){
			if (hitRes.GetActor()){
				TArray<UActorComponent*> comps;

				hitRes.GetActor()->GetComponents(comps);
				//Looks for a staic mesh component to apply the force to
				for (int i = 0; i < comps.Num(); ++i){
					UStaticMeshComponent* thisComp = Cast<UStaticMeshComponent>(comps[i]);
					if (thisComp){
						if (thisComp->IsSimulatingPhysics()){
							float force;
							force = ((PushForce * 1000.0f) / CastDistance) * (CastDistance - FVector::Dist(hitRes.GetActor()->GetActorLocation(), startPos));
							FVector forceVec = player->GetActorForwardVector() * force;
							thisComp->AddForceAtLocation(-forceVec, hitRes.Location);
						}
					}
				}
			}
		}
	}
	else{
		//Decativates the particle effect when not in use
		if (m_windParticles){
			FVector tempPos = player->GetActorLocation();
			FRotator tempRot = player->GetActorForwardVector().Rotation();

			m_windParticles->SetActorLocation(tempPos);
			m_windParticles->SetActorRotation(tempRot);

			TArray<UActorComponent*> comps;

			m_windParticles->GetComponents(comps);

			for (int i = 0; i < comps.Num(); ++i){
				UParticleSystemComponent* particleComp = Cast<UParticleSystemComponent>(comps[i]);

				if (particleComp){
					particleComp->Deactivate();
				}
			}
		}
	}

	if (m_showingMarker){
		startPos = player->GetActorLocation();
		endPos = startPos - (player->GetActorUpVector() * 1000.0f);

		//Traces a line down the ground from the centre of the player
		if (myWorld->LineTraceSingle(hitRes, startPos, endPos, ECC_WorldStatic, traceParams)){
			if (hitRes.GetActor()){
				//Moves the marker if it exists
				if (m_curMarker){
					m_curMarker->SetActorLocation(hitRes.ImpactPoint);
					m_curMarker->SetActorRotation(player->GetActorForwardVector().Rotation());
				}
			}
		}
	}
}

void UWindPushAbility::endAbility(AActor* player){
	//Add any code for when the ability ends here
}

void UWindPushAbility::stopAbility(){
	//Removes the marker
	if (m_showingMarker){
		if (m_curMarker){
			m_curMarker->Destroy();
			m_curMarker = NULL;
			m_showingMarker = false;
		}
	}

	//Deactivates the wind particle effect then deletes it
	if (m_windParticles){
		TArray<UActorComponent*> comps;

		m_windParticles->GetComponents(comps);

		for (int i = 0; i < comps.Num(); ++i){
			UParticleSystemComponent* particleComp = Cast<UParticleSystemComponent>(comps[i]);

			if (particleComp){
				particleComp->Deactivate();
			}
		}

		m_windParticles->Destroy();
		m_windParticles = NULL;
	}

	m_channeled = false;
}

void UWindPushAbility::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction){
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}