// Fill out your copyright notice in the Description page of Project Settings.

#include "MP_Whitebox.h"
#include "FireAbilitySet.h"

UFireAbilitySet::UFireAbilitySet(const class FObjectInitializer& PCIP)
: Super(PCIP){
}

void UFireAbilitySet::initialisePowers(const class FObjectInitializer& PCIP, AActor* parent){
	FireballAbility = PCIP.CreateAbstractDefaultSubobject<UFireballAbility>(parent, TEXT("Fireball"));
	//If any poblems occur in blueprint comment out the line above and use the line below, when it fails to save switch them back
	//FireballAbility = NewNamedObject<UFireballAbility>(parent, TEXT("Fireball"));
	Abilities.Add(FireballAbility);
}

void UFireAbilitySet::init(){
}

void UFireAbilitySet::startAbility(uint32 ability, AActor* player){
	switch (ability){
	case FIREBALL:
		Abilities[FIREBALL]->startAbility(player);
		break;
	default:
		break;
	}
}

void UFireAbilitySet::useAbility(uint32 ability, AActor* player){
	switch (ability){
	case FIREBALL:
		Abilities[FIREBALL]->useAbility(player);
		break;
	default:
		break;
	}
}

void UFireAbilitySet::endAbility(uint32 ability, AActor* player){
	switch (ability){
	case FIREBALL:
		Abilities[FIREBALL]->endAbility(player);
		break;
	default:
		break;
	}
}

void UFireAbilitySet::stopAbility(uint32 ability){
	switch (ability){
	case FIREBALL:
		Abilities[FIREBALL]->stopAbility();
		break;
	default:
		break;
	}
}

bool UFireAbilitySet::isChanneled(uint32 ability){
	switch (ability){
	case FIREBALL:
		Abilities[FIREBALL]->isChanneled();
		break;
	default:
		return false;
		break;
	}

	return false;
}

void UFireAbilitySet::isChanneled(uint32 ability, bool channeling){
	switch (ability){
	case FIREBALL:
		Abilities[FIREBALL]->isChanneled(channeling);
		break;
	default:
		break;
	}
}


