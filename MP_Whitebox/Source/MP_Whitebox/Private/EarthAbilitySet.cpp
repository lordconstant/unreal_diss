// Fill out your copyright notice in the Description page of Project Settings.

#include "MP_Whitebox.h"
#include "EarthAbilitySet.h"

UEarthAbilitySet::UEarthAbilitySet(const class FObjectInitializer& PCIP)
	: Super(PCIP)
{ 
}

void UEarthAbilitySet::initialisePowers(const class FObjectInitializer& PCIP, AActor* parent){
	EarthPillarAbility = PCIP.CreateAbstractDefaultSubobject<UEarthPillarAbility>(parent, TEXT("Earth Pillar"));
	//If any poblems occur in blueprint comment out the line above and use the line below, when it fails to save switch them back
	//EarthPillarAbility = NewNamedObject<UEarthPillarAbility>(parent, TEXT("Earth Pillar"));
	Abilities.Add(EarthPillarAbility);
}


void UEarthAbilitySet::init(){
}

void UEarthAbilitySet::startAbility(uint32 ability, AActor* player){
	switch (ability){
	case EARTHPILLAR:
		Abilities[EARTHPILLAR]->startAbility(player);
		break;
	default:
		break;
	}
}

void UEarthAbilitySet::useAbility(uint32 ability, AActor* player){
	switch (ability){
	case EARTHPILLAR: 
		Abilities[EARTHPILLAR]->useAbility(player);
		break;
	default:
		break;
	}
}

void UEarthAbilitySet::endAbility(uint32 ability, AActor* player){
	switch (ability){
	case EARTHPILLAR:
		Abilities[EARTHPILLAR]->endAbility(player);
		break;
	default:
		break;
	}
}

void UEarthAbilitySet::stopAbility(uint32 ability){
	switch (ability){
	case EARTHPILLAR:
		Abilities[EARTHPILLAR]->stopAbility();
		break;
	default:
		break;
	}
}

bool UEarthAbilitySet::isChanneled(uint32 ability){
	switch (ability){
	case EARTHPILLAR:
		return Abilities[EARTHPILLAR]->isChanneled();
		break;
	default:
		return false;
		break;
	}

	return false;
}

void UEarthAbilitySet::isChanneled(uint32 ability, bool channeling){
	switch (ability){
	case EARTHPILLAR:
		Abilities[EARTHPILLAR]->isChanneled(channeling);
		break;
	default:
		break;
	}
}