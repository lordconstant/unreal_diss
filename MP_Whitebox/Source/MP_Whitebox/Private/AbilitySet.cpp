// Fill out your copyright notice in the Description page of Project Settings.

#include "MP_Whitebox.h"
#include "AbilitySet.h"


UAbilitySet::UAbilitySet(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
	bAutoActivate = true;
	bAutoRegister = true;
}

void UAbilitySet::init(){
	return;
}

void UAbilitySet::startAbility(uint32 ability, AActor* player){
	return;
}

//bool UAbilitySet::useAbility_Validate(uint32 ability, AActor* player){
//	return true;
//}

void UAbilitySet::useAbility(uint32 ability, AActor* player){
	return;
}

bool UAbilitySet::endAbility_Validate(uint32 ability, AActor* player){
	return true;
}

void UAbilitySet::endAbility_Implementation(uint32 ability, AActor* player){
	return;
}

void UAbilitySet::stopAbility(uint32 ability){
	return;
}

bool UAbilitySet::isChanneled(uint32 ability){
	return false;
}

void UAbilitySet::isChanneled(uint32 ability, bool channeling){
	return;
}