// Fill out your copyright notice in the Description page of Project Settings.

#include "MP_Whitebox.h"
#include "ElementalCharacter.h"


AElementalCharacter::AElementalCharacter(const class FObjectInitializer& PCIP)
	: Super(PCIP)
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = PCIP.CreateDefaultSubobject<USpringArmComponent>(this, TEXT("CameraBoom"));
	CameraBoom->AttachTo(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = PCIP.CreateDefaultSubobject<UCameraComponent>(this, TEXT("FollowCamera"));
	FollowCamera->AttachTo(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	windAbilities = PCIP.CreateDefaultSubobject<UWindAbilitySet>(this, TEXT("WindAbilitySet"));
	earthAbilities = PCIP.CreateDefaultSubobject<UEarthAbilitySet>(this, TEXT("EarthAbilitySet"));
	fireAbilities = PCIP.CreateDefaultSubobject<UFireAbilitySet>(this, TEXT("FireAbilitySet"));
	windAbilities->initialisePowers(PCIP, this);
	earthAbilities->initialisePowers(PCIP, this);
	fireAbilities->initialisePowers(PCIP, this);

	//windAbilities->setupdatedcomponent
	changeToEarth();

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)
}

//////////////////////////////////////////////////////////////////////////
// Input

void AElementalCharacter::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	// Set up gameplay key bindings
	check(InputComponent);
	InputComponent->BindAction("SpawnObject", IE_Pressed, this, &AElementalCharacter::EarthPillar);
	InputComponent->BindAction("SpawnObject", IE_Released, this, &AElementalCharacter::releaseRightMouseButton);
	InputComponent->BindAction("WindAbilities", IE_Pressed, this, &AElementalCharacter::changeToWind);
	InputComponent->BindAction("EarthAbilities", IE_Pressed, this, &AElementalCharacter::changeToEarth);
	InputComponent->BindAction("FireAbilities", IE_Pressed, this, &AElementalCharacter::changeToFire);
	InputComponent->BindAxis("MoveForward", this, &AElementalCharacter::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &AElementalCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	InputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	InputComponent->BindAxis("TurnRate", this, &AElementalCharacter::TurnAtRate);
	InputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	InputComponent->BindAxis("LookUpRate", this, &AElementalCharacter::LookUpAtRate);

	// handle touch devices
	InputComponent->BindTouch(IE_Pressed, this, &AElementalCharacter::TouchStarted);
	InputComponent->BindTouch(IE_Released, this, &AElementalCharacter::TouchStopped);
}


void AElementalCharacter::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
	// jump, but only on the first touch
	if (FingerIndex == ETouchIndex::Touch1)
	{
		Jump();
	}
}

void AElementalCharacter::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
	if (FingerIndex == ETouchIndex::Touch1)
	{
		StopJumping();
	}
}

void AElementalCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AElementalCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AElementalCharacter::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void AElementalCharacter::MoveRight(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get right vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

void AElementalCharacter::EarthPillar(){
	if (activeAbilities){
		activeAbilities->endAbility(EARTHPILLAR, this);
		activeAbilities->isChanneled(EARTHPILLAR, true);
	}
}

void AElementalCharacter::releaseRightMouseButton(){
	if (activeAbilities){
		activeAbilities->isChanneled(EARTHPILLAR, false);
	}
}

void AElementalCharacter::changeToWind(){
	if (activeAbilities){
		activeAbilities->stopAbility(EARTHPILLAR);
	}

	activeAbilities = windAbilities;

	if (activeAbilities){
		activeAbilities->startAbility(WINDPUSH, this);
	}
}

void AElementalCharacter::changeToEarth(){
	if (activeAbilities){
		activeAbilities->stopAbility(EARTHPILLAR);
	}

	activeAbilities = earthAbilities;

	if (activeAbilities){
		activeAbilities->startAbility(EARTHPILLAR, this);
	}
}

void AElementalCharacter::changeToFire(){
	if (activeAbilities){
		activeAbilities->stopAbility(EARTHPILLAR);
	}

	activeAbilities = fireAbilities;

	if (activeAbilities){
		activeAbilities->startAbility(FIREBALL, this);
	}
}

void AElementalCharacter::Tick(float DeltaSeconds){
	Super::Tick(DeltaSeconds);

	if (activeAbilities){
		activeAbilities->useAbility(EARTHPILLAR, this);
	}
}