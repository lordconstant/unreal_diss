// Fill out your copyright notice in the Description page of Project Settings.

#include "MP_Whitebox.h"
#include "EarthPillarAbility.h"
#include "Classes/Components/DecalComponent.h"
#include "DrawDebugHelpers.h"

UEarthPillarAbility::UEarthPillarAbility(const class FObjectInitializer& PCIP)
	: Super(PCIP)
{
	//Intitalising variables - Setting defaults
	MaxPillars = 3;
	CooldownTime = 1.0f;

	m_showingMarker = false;
	m_canSpawn = false;

	m_cooldownTimer = 0.0f;

	m_dynamMat = NULL;
	m_curMarker = NULL;

	m_failTimer = 0.0f;
	m_spawnFailed = false;

	bWantsInitializeComponent = true;
}

void UEarthPillarAbility::startAbility(AActor* player){
	//Any inital jobs that the ability does can be added here
	//Only run once when ability is selected
}

void UEarthPillarAbility::useAbility(AActor* player){
	if (player->GetInstigatorController()){
		FHitResult hitRes(ForceInit);
		FCollisionQueryParams traceParams((FName)"EarthPillarTrace", true);

		traceParams.bTraceComplex = true;
		traceParams.bTraceAsyncScene = true;
		traceParams.bReturnPhysicalMaterial = false;
		traceParams.AddIgnoredActor(player);

		FVector startPos, endPos;
		FRotator tempRot;

		UWorld* myWorld = player->GetWorld();

		player->GetInstigatorController()->GetPlayerViewPoint(startPos, tempRot);
		endPos = tempRot.Vector();

		//Normalised to be prepared for multiplication by the casting distance
		endPos.Normalize();

		//Traces a ray from the players camera to the location they are looking at
		if (myWorld->LineTraceSingle(hitRes, startPos, startPos + (endPos * CastDistance), ECC_PhysicsBody, traceParams)){
			if (hitRes.GetActor()){
				//Checks to see if you hit a valid ground
				if (hitRes.GetActor()->ActorHasTag("Ground") || hitRes.GetActor()->ActorHasTag("Water")){

					//Creates a new marker or moves the existing one
					if (!m_showingMarker){
						FRotator tempRot = hitRes.ImpactNormal.Rotation();
						m_curMarker = (AActor*)myWorld->SpawnActor(PillarMarker, &hitRes.ImpactPoint, &tempRot);
						m_showingMarker = true;

						changeMarkerColor(MarkerDefaultColor);
					}
					else{
						if (m_curMarker){
							//Optimisation to avoid consistent marker spawning
							m_curMarker->SetActorLocation(hitRes.ImpactPoint);
							m_curMarker->SetActorRotation(hitRes.ImpactNormal.Rotation());
							m_pillRot = hitRes.ImpactNormal.Rotation();
						}
					}

					if (hitRes.GetActor()->ActorHasTag("Ground")){
						m_water = false;
					}
					else{
						m_water = true;
					}
				}
				else{
					stopAbility();
				}
			}
		}
		else{
			stopAbility();
		}
	}
	else{
		stopAbility();
	}
}

bool UEarthPillarAbility::endAbility_Validate(AActor* player){
	//If any pillars are non existant remove them from the array
	for (int i = 0; i < m_pillars.Num(); ++i){
		if (!m_pillars[i]){
			m_pillars.RemoveAt(i);
			i--;
		}
	}

	return true;
}

void UEarthPillarAbility::endAbility_Implementation(AActor* player){
	//Check to see if a pillar can spawn
	if (m_showingMarker && m_canSpawn && Pillar && m_curMarker){

		//If the angle is too steep a pillar wont spawn -- If any problems arise convert to angle !A!
		if (m_curMarker->GetActorUpVector().Z > 0.75f){
			return;
		}

		FVector tempLoc = m_curMarker->GetActorLocation();
		FRotator tempRot = FRotator(0, 0, 0);

		AActor* newPillar = player->GetWorld()->SpawnActor(Pillar, &tempLoc, &tempRot);

		FBox checkBox;
		FVector boxOrigin, boxExtents, tempExtents, tempOrigin;

		newPillar->GetActorBounds(true, boxOrigin, boxExtents);

		player->GetActorBounds(true, tempOrigin, tempExtents);

		checkBox = FBox::BuildAABB(tempLoc, boxExtents + (tempExtents*2));

		//Check to see if player is too close to pillar
		if (checkBox.IsInside(player->GetActorLocation())){
			newPillar->Destroy();
			changeMarkerColor(MarkerFailColor);

			m_spawnFailed = true;
			m_failTimer = 0.0f;

			return;
		}

		//Checks to see if the new pillar will collides with any existing ones
		for (int i = 0; i < m_pillars.Num(); ++i){
			FBox tempBox;
			FVector tempBoxOrigin, tempBoxExtents;

			m_pillars[i]->GetActorBounds(true, tempBoxOrigin, tempBoxExtents);

			tempBox = FBox::BuildAABB(tempBoxOrigin, tempBoxExtents);

			checkBox = FBox::BuildAABB(boxOrigin, boxExtents);

			//Removes the new pillar if a collision is made
			if (checkBox.Intersect(tempBox)){
				newPillar->Destroy();

				changeMarkerColor(MarkerFailColor);

				m_spawnFailed = true;
				m_failTimer = 0.0f;

				return;
			}
		}

		//Old scaling code - remove when decision to remove has been made
		/*FVector tempScale = newPillar->GetActorScale3D();
		tempScale.Z = 0.0f;
		newPillar->SetActorScale3D(tempScale);*/

		//Spawns in ground particle effect --- Add in ability to change depending on surface !A!

		if (!m_water && PillarGroundParticle){
			m_groundParticles.Add(player->GetWorld()->SpawnActor(PillarGroundParticle, &tempLoc, &tempRot));
		}
		else if(WaterGroundParticle){
			m_groundParticles.Add(player->GetWorld()->SpawnActor(WaterGroundParticle, &tempLoc, &tempRot));
		}

		//Moves the new pillar below the current terrain point ready to rise
		FVector tempPos = newPillar->GetActorLocation();
		tempPos.Z -= boxExtents.Z*2;
		newPillar->SetActorLocation(tempPos);

		m_spawningPillars.Add(newPillar);
		m_pillars.Add(newPillar);

		//Clean up the marker - Move to function !A!
		m_curMarker->Destroy();
		m_curMarker = NULL;
		m_showingMarker = false;

		m_cooldownTimer = 0.0f;

		return;
	}
}

void UEarthPillarAbility::stopAbility(){
	//Cleans away the marker when the ability is ended
	if (m_showingMarker){
		if (m_curMarker){
			m_curMarker->Destroy();
			m_curMarker = NULL;
		}
		m_showingMarker = false;
	}
}

bool UEarthPillarAbility::spawnPillar_Validate(uint32 num, float deltaTime){
	return true;
}

void UEarthPillarAbility::spawnPillar_Implementation(uint32 num, float deltaTime){
	/*if (m_spawningPillars[num]->GetActorScale3D().Z == 1){
		m_spawningPillars.RemoveAt(num);

		return;
	}*/

	FVector extents, origin;

	m_spawningPillars[num]->GetActorBounds(true, origin, extents);

	FHitResult hitRes;
	FCollisionQueryParams traceParams((FName)"PillarToGround", true);

	traceParams.bTraceComplex = true;
	traceParams.bTraceAsyncScene = true;
	traceParams.bReturnPhysicalMaterial = false;
	traceParams.AddIgnoredActor(m_spawningPillars[num]);

	FVector startPos, endPos;
	FRotator tempRot;

	UWorld* myWorld = m_spawningPillars[num]->GetWorld();

	startPos = origin;
	startPos.Z -= extents.Z;
	endPos = startPos;
	endPos.Z -= extents.Z;

	//Trace below the pillar checking for the ground
	if (myWorld->LineTraceSingle(hitRes, startPos, endPos, ECC_PhysicsBody, traceParams)){
		if (hitRes.Actor->ActorHasTag("Ground")){
			m_spawningPillars.RemoveAt(num);
			//m_groundParticles[num]->Destroy();
			//m_groundParticles.RemoveAt(num);

			//Finds the particle system and deactivates it
			TArray<UActorComponent*> comps;
			m_groundParticles[num]->GetComponents(comps);

			for (int i = 0; i < comps.Num(); ++i){
				UParticleSystemComponent* particleComp = Cast<UParticleSystemComponent>(comps[i]);

				if (particleComp){
					particleComp->Deactivate();
				}
			}

			m_groundParticles.RemoveAt(num);
			return;
		}
	}

	//Moves the pillar up at the same rate no matter the size
	float riseSpeed = (extents.Z * 2);

	FVector tempPos = m_spawningPillars[num]->GetActorLocation();
	tempPos.Z += deltaTime * riseSpeed;
	m_spawningPillars[num]->SetActorLocation(tempPos);

	/*FVector tempScale = m_spawningPillars[num]->GetActorScale3D();
	tempScale.Z += deltaTime;

	if (tempScale.Z > 1.0f){
		tempScale.Z = 1.0f;
	}

	m_spawningPillars[num]->SetActorScale3D(tempScale);*/
}

bool UEarthPillarAbility::removePillar_Validate(uint32 num, float deltaTime){
	return true;
}

void UEarthPillarAbility::removePillar_Implementation(uint32 num, float deltaTime){
	/*if (m_pillars[num]->GetActorScale3D().Z == 0){
		m_pillars[num]->Destroy();
		m_pillars.RemoveAt(num);

		return;
	}*/

	//If the pillar getting removed is among the pillars spawning in, remove it from the spawn list
	for (int i = 0; i < m_spawningPillars.Num(); i++){
		if (m_spawningPillars[i] == m_pillars[num]){
			m_spawningPillars.RemoveAt(i);

			m_groundParticles[i]->Destroy();
			m_groundParticles.RemoveAt(i);
			return;
		}
	}

	FVector extents, origin;
	m_pillars[num]->GetActorBounds(true, origin, extents);

	FHitResult hitRes;
	FCollisionQueryParams traceParams((FName)"PillarToGround", true);

	traceParams.bTraceComplex = true;
	traceParams.bTraceAsyncScene = true;
	traceParams.bReturnPhysicalMaterial = false;
	traceParams.AddIgnoredActor(m_pillars[num]);

	FVector startPos, endPos;
	FRotator tempRot;

	UWorld* myWorld = m_pillars[num]->GetWorld();

	startPos = origin;
	startPos.Z += extents.Z * 5;
	endPos = origin;
	endPos.Z += extents.Z;

	//A trace starting from a little above the pillar going down to the top of the pillar
	if (myWorld->LineTraceSingle(hitRes, startPos, endPos, ECC_PhysicsBody, traceParams)){
		if (hitRes.Actor->ActorHasTag("Ground")){
			m_pillars[num]->Destroy();
			m_pillars.RemoveAt(num);

			return;
		}
	}

	//Moves the pillar down at the same rate no matter the size
	float riseSpeed = (extents.Z * 2);

	FVector tempPos = m_pillars[num]->GetActorLocation();
	tempPos.Z -= deltaTime * riseSpeed;
	m_pillars[num]->SetActorLocation(tempPos);
	
	/*FVector tempScale = m_pillars[num]->GetActorScale3D();
	tempScale.Z -= deltaTime;

	if (tempScale.Z < 0.0f){
		tempScale.Z = 0.0f;
	}

	m_pillars[num]->SetActorScale3D(tempScale);*/
}

void UEarthPillarAbility::changeMarkerColor(FColor markerCol){
	//Finds the decal on the marker actor and alters the color -- Currently causes a crash on network !A!
	/*if (m_curMarker && GetWorld() && GetOwner()){
		TArray<UActorComponent*> comps;
		
		m_curMarker->GetComponents(comps);

		for (int i = 0; i < comps.Num(); ++i){
			UDecalComponent* decalComp = Cast<UDecalComponent>(comps[i]);

			if (decalComp){
				if (!m_dynamMat){
					m_dynamMat = UMaterialInstanceDynamic::Create(decalComp->GetMaterial(0), this);
				}

				if (m_dynamMat){
					m_dynamMat->SetVectorParameterValue("GlowColor", markerCol);

					decalComp->SetMaterial(0, m_dynamMat);
				}
			}
		}
	}*/
}

void UEarthPillarAbility::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction){
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	//If there are too many pillars this removes the extras
	if (m_pillars.Num() > MaxPillars){
		for (int i = 0; i < m_pillars.Num() - MaxPillars; i++){
			removePillar(i, DeltaTime);
		}
	}

	//Rises the pillars from the ground
	for (int i = 0; i < m_spawningPillars.Num(); ++i){
		spawnPillar(i, DeltaTime);
	}

	//handles the cooldown of the pillar spawning
	if (m_cooldownTimer >= CooldownTime){
		m_canSpawn = true;
	}
	else{
		m_cooldownTimer += DeltaTime;
		m_canSpawn = false;
	}

	//Changes the color of the marker on spawn failure
	if (m_spawnFailed){
		if (m_failTimer >= MarkerFailTime){
			changeMarkerColor(MarkerDefaultColor);
			m_spawnFailed = false;
			m_failTimer = 0.0f;
		}
		else{
			m_failTimer += DeltaTime;
		}
	}
}