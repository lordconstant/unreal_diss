// Fill out your copyright notice in the Description page of Project Settings.

#include "MP_Whitebox.h"
#include "WindAbilitySet.h"


UWindAbilitySet::UWindAbilitySet(const class FObjectInitializer& PCIP)
	: Super(PCIP){
}

void UWindAbilitySet::initialisePowers(const class FObjectInitializer& PCIP, AActor* parent){
	WindPushAbility = PCIP.CreateAbstractDefaultSubobject<UWindPushAbility>(parent, TEXT("Wind Push"));
	//If any poblems occur in blueprint comment out the line above and use the line below, when it fails to save switch them back
	//WindPushAbility = NewNamedObject<UWindPushAbility>(parent, TEXT("Wind Push"));
	Abilities.Add(WindPushAbility);
}

void UWindAbilitySet::init(){
}

void UWindAbilitySet::startAbility(uint32 ability, AActor* player){
	switch (ability){
	case WINDPUSH:
		Abilities[WINDPUSH]->startAbility(player);
		break;
	default:
		break;
	}
}

void UWindAbilitySet::useAbility(uint32 ability, AActor* player){
	switch (ability){
	case WINDPUSH:
		Abilities[WINDPUSH]->useAbility(player);
		break;
	default:
		break;
	}
}

void UWindAbilitySet::endAbility(uint32 ability, AActor* player){
	switch (ability){
	case WINDPUSH:
		Abilities[WINDPUSH]->endAbility(player);
		break;
	default:
		break;
	}
}

void UWindAbilitySet::stopAbility(uint32 ability){
	switch (ability){
	case WINDPUSH:
		Abilities[WINDPUSH]->stopAbility();
		break;
	default:
		break;
	}
}

bool UWindAbilitySet::isChanneled(uint32 ability){
	switch (ability){
	case WINDPUSH:
		return Abilities[WINDPUSH]->isChanneled();
		break;
	default:
		return false;
		break;
	}

	return false;
}

void UWindAbilitySet::isChanneled(uint32 ability, bool channeling){
	switch (ability){
	case WINDPUSH:
		Abilities[WINDPUSH]->isChanneled(channeling);
		break;
	default:
		break;
	}
}